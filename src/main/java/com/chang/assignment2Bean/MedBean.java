/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chang.assignment2Bean;

import java.sql.Timestamp;
import java.util.Objects;
import javafx.beans.property.*;

/**
 *
 * @author cha_xi
 */
public class MedBean {

    private IntegerProperty ID;
    private IntegerProperty patientID;
    private ObjectProperty<Timestamp> dateOfMed;
    private StringProperty dateOfMedStr;
    private StringProperty med;
    private DoubleProperty unitCost;
    private DoubleProperty units;
    public static StringProperty sampleInfoProperty = new SimpleStringProperty("dateOfMed, med, unitCost, units");

    public MedBean(int ID, int patientID, Timestamp dateOfMed, String med, double unitCost, double units) {
        super();
        this.ID = new SimpleIntegerProperty(ID);
        this.patientID = new SimpleIntegerProperty(patientID);
        this.dateOfMed = new SimpleObjectProperty<>(dateOfMed);
        this.dateOfMedStr = new SimpleStringProperty(dateOfMed.toString());
        this.med = new SimpleStringProperty(med);
        this.unitCost = new SimpleDoubleProperty(unitCost);
        this.units = new SimpleDoubleProperty(units);
        
    }

    public MedBean() {
        this.ID = new SimpleIntegerProperty();
        this.patientID = new SimpleIntegerProperty();
        this.dateOfMed = new SimpleObjectProperty<>();
        this.dateOfMedStr = new SimpleStringProperty();
        this.med = new SimpleStringProperty();
        this.unitCost = new SimpleDoubleProperty();
        this.units = new SimpleDoubleProperty();
    }
    

    public int getID() {
        return ID.get();
    }
    
    public IntegerProperty getIDProperty() {
        return ID;
    }
    public void setID(int ID) {
        this.ID.set(ID);
    }

    public int getPatientID() {
        return patientID.get();
    }

    public IntegerProperty getPatientIDProperty() {
        return patientID;
    }    
    
    public void setPatientID(int patientID) {
        this.patientID.set(patientID);
    }

    public Timestamp getDateOfMed() {
        return dateOfMed.get();
    }

    public ObjectProperty<Timestamp> getDateOfMedProperty() {
        return dateOfMed;
    }    
    public StringProperty gedDateOfMedPropertyStr(){
        dateOfMedStr.set(dateOfMed.toString());
        return dateOfMedStr;
    }
    public void setDateOfMed(Timestamp dateOfMed) {
        this.dateOfMed.set(dateOfMed);
    }

    public String getMed() {
        return med.get();
    }

    public StringProperty getMedProperty() {
        return med;
    }    
    
    public void setMed(String med) {
        this.med.set(med);
    }

    public double getUnitCost() {
        return unitCost.get();
    }

    public DoubleProperty getUnitCostProperty() {
        return unitCost;
    }    
    
    public void setUnitCost(double unitCost) {
        this.unitCost.set(unitCost);
    }

    public double getUnits() {
        return units.get();
    }

    public DoubleProperty getUnitsProperty() {
        return units;
    }    
    
    public void setUnits(double units) {
        this.units.set(units);
    }

    @Override
    public String toString() {
        return "MedBean{" + "ID=" + ID.get() + ", patientID=" + patientID.get() 
                + ", dateOfMed=" + dateOfMed.get().toString() + ", med=" + med.get()
                + ", unitCost=" + unitCost.get() + ", units=" + units.get() + '}';
    }

    
public StringProperty getInfoProperty(){
    StringProperty sp = new SimpleStringProperty();
    sp.set("dateOfMed=" + dateOfMed.get().toString() + ", med=" + med.get()
                + ", unitCost=" + unitCost.get() + ", units=" + units.get());
    return sp;
}
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + this.ID.get();
        hash = 67 * hash + this.patientID.get();
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final MedBean other = (MedBean) obj;
        if (this.ID.get() != other.ID.get()) {
            return false;
        }
        if (this.patientID.get() != other.patientID.get()) {
            return false;
        }
        if (!Objects.equals(this.dateOfMed.get(), other.dateOfMed.get())) {
            return false;
        }
        if (!Objects.equals(this.med.get(), other.med.get())) {
            return false;
        }
        if (Double.doubleToLongBits(this.unitCost.get()) != Double.doubleToLongBits(other.unitCost.get())) {
            return false;
        }
        if (Double.doubleToLongBits(this.units.get()) != Double.doubleToLongBits(other.units.get())) {
            return false;
        }
        return true;
    }

}
